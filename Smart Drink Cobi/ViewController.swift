//
//  ViewController.swift
//  Smart Drink Kobi
//
//  Created by Dennis Menge on 27.10.18.
//  Copyright © 2018 Dennis Menge. All rights reserved.
//

import UIKit
import WebKit
import HealthKit
import Moscapsule

class ViewController: UIViewController, WKUIDelegate {

    @IBOutlet weak var kobiWebView: WKWebView!
    
    var mqttClient: MQTTClient!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view, typically from a nib.
        setupKobiWebView()
        
        let healthKitStore = setupHealthKit()
        
        connectToMqttBroker(healthStore: healthKitStore!)
    }
    
    func connectToMqttBroker(healthStore: HKHealthStore) {
        let clientId = "SmartDrink-Cobi-App"
        
        let mqttConfig = MQTTConfig(clientId: clientId, host: "test.mosquitto.org", port: 1883, keepAlive: 60)
        mqttConfig.onConnectCallback = { returnCode in
            print("Return Code is \(returnCode.description)")
        }
        mqttConfig.onMessageCallback = { mqttMessage in
            print("MQTT Message received: payload=\(mqttMessage.payloadString)")
            let messageData = mqttMessage.payloadString?.data(using: .utf8)
            let decoder = JSONDecoder()
            let drink = try! decoder.decode(Drink.self, from: messageData!)
            print("Incoming Drink: \(drink)")
           
            if let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryWater) {
                if(drink.bottleId == "911") {
                let date = Date()
                let quantity = HKQuantity(unit: HKUnit.liter(), doubleValue: drink.amount / 1000)
                let sample = HKQuantitySample(type: type, quantity: quantity, start: date, end: date)
                healthStore.save(sample, withCompletion: { (success, error) in
                    print("Saved \(success), error \(error)")
                })}
            }
            
        }
    
        // create new MQTT Connection
        mqttClient = MQTT.newConnection(mqttConfig)
        
        // publish and subscribe
        mqttClient.subscribe("DrinkSmart", qos: 1)

        }
    
    
    func setupHealthKit() -> HKHealthStore? {
        let healthStore = HKHealthStore()
        let allTypes = Set([
                            HKObjectType.quantityType(forIdentifier: .dietaryWater)!])
        
        healthStore.requestAuthorization(toShare: allTypes, read: allTypes) { (success, error) in
            if !success {
                // Handle the error here.
                print("HealthKit permission have not been granted")
            }
        }
       return healthStore
    }

    func setupKobiWebView() {
        let dashboardUrl = URL(string: "https://enigmatic-dawn-85841.herokuapp.com/cobi/index.html")
        let dashboardUrlRequest = URLRequest(url: dashboardUrl!)
        kobiWebView.load(dashboardUrlRequest)
    }

}

extension WKWebView {
    override open var safeAreaInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}

struct Drink: Decodable {
    let bottleId: String
    let amount: Double
    let unit: String
}
